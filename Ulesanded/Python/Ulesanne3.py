#!/usr/bin/python3

import random

##############################
#
# A number guessing game.
#
##############################i

guessable = random.randint(0, 100)
guess = int(input('A number guessing game. Number is between 0 and 100 inclusive.\nYour guess: '))
while guess != guessable:
	print('Wrong answer! Your guess is too', end=' ')
	if guess < guessable:
		print('small.')
	else:
		print('big.')
	guess = int(input('Your guess: '))
print('%i is the right answer.' % guessable)
