#!/usr/bin/python3
##################################################
#
# Multiplication table. Requires Python 3 to run.
#
##################################################

maxnum =int(input('This program prints out multiplication table.\nInsert a number to determine the size of the multiplication table!\nNumber: '))

for i in range(1, maxnum+1):
	for j in range(1, maxnum+1):
		print(i * j, end='\t')
	print('\n')
