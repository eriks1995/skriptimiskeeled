#!/usr/bin/python3

#################################
#
# Kalkulaator! Ei vaja kaivitamiseks erioigusi ega lisamooduleid
#
#################################

print('Kalkulaator!\nPalun valige operatsioon:\n\t1. Liitmine\n\t2. Lahutamine\n\t3. Korrutamine\n\t4. Jagamine\n\t5. Astendamine\n\t6. Juurimine')
valik = input('Valiku number: ')

if valik is '1':
	esim = tein = 'liidetav'
	mark = '+'
elif valik is '2':
	esim = 'vahendatav'
	tein = 'vahendaja'
	mark = '-'
elif valik is '3':
	esim = tein = 'tegur'
	mark = '*'
elif valik is '4':
	esim = 'jagatav'
	tein = 'jagaja'
	mark = '/'
elif valik is '5':
	esim = 'astendatav'
	tein = 'astendaja'
	mark = '**'
elif valik is '6':
	esim = 'juuritav'
	tein = 'juurija'
	mark = '**'
else:
	print('Viga valiku tegemisel!\n')
	quit()

oper1 = input('Esimene operand(' + esim + '): ')
oper2 = input('Teine operand(' + tein + '): ')

if valik is '6':
	oper2 = '(1/' + oper2 + ')'

vastus = eval(oper1 + mark + oper2)
print('%.2f' % vastus)
