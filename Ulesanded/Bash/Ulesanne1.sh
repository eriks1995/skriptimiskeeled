#!/bin/bash

# kõikide järgnevate käskude stdout ja stderr saadetakse /dev/nulli
# kasutada skripti käivitamiseks sudo õigusi

# kontrollib, kas samba on paigaldatud
dpkg -s samba > /dev/null  2>&1

# kui eelneva käsu veakood pole 0 (samba pole paigaldatud)
if [ $? != 0 ]
	then
	dpkg --configure -a > /dev/null 2>&1 && apt -fy install > /dev/null 2>&1 && apt -y update > /dev/null 2>&1 && apt -y install samba > /dev/null 2>&1 && apt clean > /dev/null 2>&1

	# kui samba paigaldamisel esines tõrge
	if [ $? != 0 ]
		then
		echo -e "Viga Samba paigaldamisel!\n"
		exit 1
	fi
fi

# kontrollib, kas kaust on juba olemas
test -d $1 > /dev/null 2>&1

# juhul, kui eelneva käsu veakood pole 0 (kausta pole olemas)
if [ $? != 0 ]
	then
	mkdir -p $1 > /dev/null 2>&1

	# kui kausta loomisel esines tõrge
	if [ $? != 0 ]
		then
		echo -e "Viga kausta loomisel!\n"
		exit 1
	fi
fi

# loome kausta jaoks absolute pathi
if [[ "$1" == /* ]]
	then
	abs_path=$1
else
	this_path=$(pwd) > /dev/null 2>&1
	abs_path=$this_path'/'$1
fi

# kontrollib, kas grupp on juba olemas
getent group | cut -d: -f1 | grep $2 > /dev/null 2>&1

# juhul, kui eelneva käsu veakood pole 0 (gruppi pole olemas)
if [ $? != 0 ]
	then
	addgroup --force-badname $2 > /dev/null 2>&1

	# kui grupi loomisel esines tõrge
	if [ $? != 0 ]
		then
		echo -e "Viga grupi loomisel!\n"
		exit 1
	fi
fi

# kontrollime, kas grupil on share olemas Samba konfiguratsioonifailis
grep -F "[$2]" /etc/samba/smb.conf > /dev/null 2>&1

# juhul, kui eelneva käsu exit code pole 0 (share'i pole olemas)
if [ $? != 0 ]
	then
	echo -e "\n[$2]\ncomment = $2 share\npath = $abs_path\nwritable = yes\nvalid users = @$2\nforce group = $2\nbrowsable = yes" >> /etc/samba/smb.conf

	# kui share'i lisamisel konfiguratsioonifaili esines tõrge
	if [ $? != 0 ]
		then
		echo -e "Viga samba seadistamisel!\n"
	fi
fi

# teeme Sambale restardi, et konfiguratsioon jõustuks
service smbd restart
