# Kalkulaator. Küsib kasutajalt 1. ja 2. muutuja, ja teostatava operatsiooni (liitmine, lahutamine, korrutamine või jagamine).
# Kõikide käskude stdout ja stderr saadetakse faili /dev/null

read -p "Kalkulaator!
Palun valige operatsioon:
	1. Liitmine
	2. Lahutamine
	3. Korrutamine
	4. Jagamine
Valiku number: " valik

if [ $valik = 1 ]
	then
	symbol='+'
elif [ $valik = 2 ]
	then
	symbol='-'
elif [ $valik = 3 ]
	then
	symbol="*"
elif [ $valik = 4 ]
	then
	symbol='/'
else
	echo -e "Viga valiku tegemisel!\n"
	exit 1
fi

read -p "Esimene operand: " esimene
read -p "Teine operand: " teine

let vastus="$esimene $symbol $teine"

echo "$esimene $symbol $teine = $vastus"
