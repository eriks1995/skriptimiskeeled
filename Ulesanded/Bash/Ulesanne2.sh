#!/bin/sh

# kõikide järgnevate käskude stdout ja stderr saadetakse faili /dev/null
# kasutada skripti käivitamiseks sudo õigusi

read -p "Skript paigaldab Teie arvutisse Giti, loob varemloodud BitBucketi repositooriumijaoks kodukataloogi kausta ning sünkroniseerib viimase. Kas soovite skripti käivitada (jah/ei)?" prompt

# kui skripti ei soovita käivitada
if [ $prompt = "ei" ]
	then
	exit 0
# kui küsimuse vastus pole 'ei' ega 'jah'
elif [ $prompt != "jah" ]
	then
	exit 1
fi

# kontrollime, kas git on paigaldatud
dpkg -s git > /dev/null 2>&1

# kui git pole paigaldatud
if [ $? != 0 ]
	then
	apt -y update > /dev/null 2>&1
	apt -y install git > /dev/null 2>&1
	
	if [ $? != 0 ]
		then
		echo 'Tõrge giti paigaldamisel!'
		exit 1
	fi
	
	apt clean > /dev/null 2>&1
fi

read -p "Palun öelge giti jaoks loodava kausta nimi: " kaustanimi

# loome giti jaoks kodukataloogi soovitud nimega kausta
mkdir ~/$kaustanimi > /dev/null 2>&1

if [ $? != 0 ]
	then
	echo 'Tõrge kausta loomisel!'
	exit 1
fi

cd ~/$kaustanimi

# loome loodud kausta giti repositooriumi
git init > /dev/null 2>&1
if [ $? != 0 ]
	then
	echo 'Tõrge giti repositooriumi loomisel!'
	exit 1
fi

git config --global user.name "Erik Sõlg" > /dev/null 2>&1
git config --global user.email "eriks1995@gmail.com" > /dev/null 2>&1

# ühendame repositooriumi BitBucketi omaga
git remote add origin https://eriks1995@bitbucket.org/Martinra_itk/skriptimine_2017.git > /dev/null 2>&1
if [ $? != 0 ]
	then
	echo 'Tõrge BitBucketi reositooriumiga ühendamisel'
	exit 1
fi

# sünkroniseerime kausta sisu BitBucketi repositooriumiga
git pull origin master
if [ $? != 0 ]
	then
	echo 'Tõrge repositooriumi sünkroniseerimisel!'
	exit 1
fi
