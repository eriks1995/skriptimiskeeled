#/bin/bash
# See skript võtab sisendiks kaustajanime ja arvutab temale allokeeritud (VSZ) ja kasutuses oleva (RSS) mälukoguse
# Koikide käskude stdout ja stderr saadetakse faili /dev/null

read -p "Sisestage kasutajanimi, kelle mälukasutust soovite jälgida: " kasutaja

# kontrollime, kas kasutaja on olemas
cat /etc/passwd | awk -F':' '{print $1}' | grep -w "$kasutaja" > /dev/null 2>&1

# kui kasutajat pole
if [ $? != 0 ]
	then
	echo -e "Sellist kasutajat pole olemas\n"
	exit 1
fi

# kasutame käsku 'ps', et saada kätte VSZ ja RSS väärtused
rss=`ps -e -o user,rss,vsz | grep $kasutaja | awk '{print $2}'`
vsz=`ps -e -o user,rss,vsz | grep $kasutaja | awk '{print $3}'`

# loome muutujad VSZ ja RSS väärtuste ja ühikute hoidmiseks
rss_kokku=0
rss_yhik="KiB"
vsz_kokku=0
vsz_yhik="KiB"

# liidame kokku RSS väärtused
for i in $rss
do
	let rss_kokku=rss_kokku+$i
done

# liidame kokku VSZ väärtused
for i in $vsz
do
	let vsz_kokku=vsz_kokku+$i
done

# anname RSS koguväärtusele kasutajasõbraliku ühiku
for i in MiB GiB TiB
do
	if [ $rss_kokku -gt 1023 ]
		then
		let rss_kokku=rss_kokku/1024
		rss_yhik=$i
	else
		break
	fi
done

# anname VSZ koguväärtusele kasutajasõbraliku ühiku
for i in MiB GiB TiB
do
	if [ $vsz_kokku -gt 1023 ]
		then
		let vsz_kokku=vsz_kokku/1024
		vsz_yhik=$i
	else
		break
	fi
done

echo -e "Kasutajale $kasutaja on kokku allokeeritud mälu $vsz_kokku $vsz_yhik, millest kasutuses on $rss_kokku $rss_yhik\n"
