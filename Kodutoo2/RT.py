#!/usr/bin/python3
###########################################################
#
# Search engine for Rotten Tomatoes film review aggregator.
# Requires Python 3 to run.
#
# Overview:
# 1. Check of required modules and packages. Installation,
#    when needed.
# 2. Prompting user of, whether movies to be searched are
#    in theatres at the moment or already on DVD.
# 3. Then, an user is prompted for information about the
#    movies to be searched (ratings, genres, years, etc).
#    Function called generate_url() deals with it.
# 4. With the acquired information, an URL of Rotten
#    Tomatoes website is generated (in most cases, with
#    generate_url() function).
# 5. From the web page a source code containing data about
#    the movies is downloaded.
# 6. Data is extracted from html code in one of two ways:
#	1. A portion of JSON code is extracted from source
#	   code which then will be decoded. Function
#	   get_movie_data() does that.
#	2. Using BeautifulSoup library to get movie data
#	   from between defined html tags. Function
#	   soup_movies() does that.
# 7. In case of JSON code, a data is displayed to stdout
#    with function display_movies().
#
###########################################################




###########################################################
#
# Check of prerequisites. Installation, when needed.
#
###########################################################

import os
import sys
import subprocess
import json
import urllib.request
import datetime

print("Checking prerequisites...\n")

result = int(subprocess.check_output('dpkg -s python3-pip > /dev/null 2>&1; echo $?', shell=True))

if result != 0:
	if int(os.getuid()) != 0:
		print("First time run needs root privileges! Exiting.\n")
		quit()
	os.system("apt update > /dev/null 2>&1")
	os.system("apt install python3-pip > /dev/null 2>&1")

if 'pick' not in sys.modules:
	os.system("pip3 install pick > /dev/null 2>&1")
if 'beautifulsoup4' not in sys.modules:
	os.system("pip3 install beautifulsoup4 > /dev/null 2>&1")

from pick import pick
from bs4 import BeautifulSoup




###########################################################
#
# generate_url()
# Parameters:
#	filter_type - type of search to be performed.
#		       	"theaters" for movies currently
#		       	in theaters, "custom" for movies
#			already on DVDs (custom search).
# Returns: an URL of Rotten Tomatoes webpage where
# 	   movies searched for are listed. URL is created
# 	   with the use of GET variables.
#
# Contains def_value() which is used with input() method
# 	   to select a default value in case no value is
#    	   entered.
#
###########################################################

def generate_url(filter_type):
	
	def def_value (string, value):
		if len(string) > 0:
			if string[-1] == '%':
				return string[:len(string)-1]
			else:
				return string
		else:
			return value
	
	try:
		url_min_tomato = int(def_value(input("\nMinimum tomatometer score (0): "), "0"))
		url_max_tomato = int(def_value(input("\nMaximum tomatometer score (100): "), "100"))
		if url_max_tomato < url_min_tomato:
			print("\nMaximum score cannot be lower than minimum! Exiting.\n")
			quit()
	
		if filter_type == "theaters":
			url_min_public = int(def_value(input("\nMinimum audience score (0): "), "0"))
			url_max_public = int(def_value(input("\nMaximum audience score (100): "), "100"))
		
			if url_max_public < url_min_public:
				print("\nMaximum score cannot be lower than minimum! Exiting.\n")
				quit()
	except ValueError:
		print("\nScore must be an integer! Exiting.\n")
		quit()

	picked_genres = pick(['Action','Animation','Art & Foreign','Classics','Comedy','Documentary','Drama','Horror','Kids & Family','Mystery','Romance','Sci-fi & Fantasy'],
			"Please choose one or more genres (Space) and press Enter",
			multi_select=True, min_selection_count=1)	
	genres_dict = {0:"1", 1:"2", 2:"4", 3:"5", 4:"6", 5:"8", 6:"9", 7:"10", 8:"11", 9:"13", 10:"18", 11:"14"}
	url_genres = ""
	for genre in picked_genres:
		url_genres = url_genres + genres_dict[genre[1]] + ";"
	url_genres =url_genres[:len(url_genres)-1]	
	
	sorting = input("Sorting:\n\t1. Release date\n\t2. Tomatometer\nYour choice (1): ")
	if sorting == "1" or sorting == "":
		url_sorting = "release"
	elif sorting == "2":
		url_sorting = "tomato"
	else:
		print("\nInvalid option! Exiting.\n")
		quit()
	
	url = "https://www.rottentomatoes.com/browse/"
	if filter_type == "theaters":
		url = url + "in-theaters?"
	elif filter_type == "custom":
		url = url + "dvd-all?"
	url = url + "minTomato=" + str(url_min_tomato) + "&maxTomato=" + str(url_max_tomato)
	if filter_type == "theaters":
		url = url + "&minPopcorn=" + str(url_min_public) + "&maxPopcorn=" + str(url_max_public)
	elif filter_type == "custom":
		url = url + "&services=amazon;hbo_go;itunes;netflix_iw;vudu;amazon_prime;fandango_now"
	url = url + "&genres=" + url_genres + "&sortBy=" + url_sorting
	
	return url




###########################################################
#
# get_movie_data()
# Parameters:
#	url - an address of Rotten Tomatoes web page that
# 	      contains movie data represented by JSON code.
# Returns: decode JSON code that contains movies and their
#  	   parameters (name, release date, etc).
#
###########################################################

def get_movie_data(url):
	html = urllib.request.urlopen(url)
	data = ""

	in_data = False

	for line in html:
		line = line.decode('utf-8')
		if "[{\"id\":" in line and in_data == False:
			in_data = True
		if in_data == True:
			data = data + line
		if ']}],' in line and in_data == True:
			in_data = False

	data = data.strip()
	data = data[:-1]
	return json.loads(data)




###########################################################
#
# dispaly_movies()
# Parameters:
#	movie_data - movie data represented by JSON code.
# Function displays movie data in user-friendly stdout
#   output. Used, when movie data resides in JSON code.
#
###########################################################

def display_movies(movie_data):
	number = 1
	print("\n")
	for movie in movie_data:
		try:
			print(str(number) + ". " + movie['title'] + "\n\tTomatometer score: " + str(movie['tomatoScore']) + "\n\tPublic score: " + str(movie['popcornScore']) + "\n\tRelease date: " + movie['theaterReleaseDate']  + "\n")
		except KeyError:
			print(str(number) + ". " + movie['title'] + "\n\tTomatometer score: " + str(movie['tomatoScore']) + "\n\tPublic score: " + str(movie['popcornScore']) + "\n\tRelease date: " + movie['dvdReleaseDate']  + "\n")
		number = number + 1




###########################################################
#
# soup_movies()
# Parameters:
#	url - an address of Rotten Tomatoes web page that
#	      contains a movie info.
# Function displays movie data collected from web page with
#   the help of BeautifulSoup library in user-friendly
#   stdout output.
#
###########################################################

def soup_movies(url):
	html = urllib.request.urlopen(url)
	soup = BeautifulSoup(html, "html.parser")
	number = 1
	print("\n")
	for movie in soup.find_all("a", "unstyled articleLink"):
		if movie.parent.name == "td":
			print(str(number) + ". " + movie.string.strip())
			number = number + 1




###########################################################
#
#  Main code. Structure will be the following:
#  1. Ask user, whether the movies to be searched will be
#     in theaters or not.
#  2. Meanwhile, we start constructing an URL for the page
#     that contains the movie data.
#  3. One of the following will be implemented based on the
#     user's answers:
#	- We scan for movies in theaters. With the help of
#	  user's answers, we use generate_url() to generate
#	  an URL , get json data with get_movie_data()
#	  method and display acquired data with
#	  display_movies() method.
#	- We scan for movies already on DVDs. In that case
#	  we have 2 options:
#		- Create a custom search for movies.
#		  generate_url(), get_movie_data() and 
#		  display_movies() will be used again.
#		- Display top movies based on critic
#		  reviews. In that case, top movies of all
#		  time, top movies by genre and top movies
#		  by year can be choosed. In all 3 options
#		  generate_url() method is not used for URL
#		  generation, so as get_movie_data() and 
#		  display_movies(). soup_movies() method
#		  will be used for data acquiring and 
#		  displaying instead.
#
###########################################################

in_theaters = input("Movies in theatres (yes)? ").lower()

if in_theaters == "yes" or in_theaters == "y" or in_theaters == "":
	url = generate_url("theaters")
	movie_data = get_movie_data(url)
	display_movies(movie_data)

elif in_theaters == "no" or in_theaters == "n":
	search_option = input("\nPlease define a method for selecting movies:\n\t1. Custom search\n\t2. Top movies\nYour choice (1): ")
	if search_option == "1" or search_option == "":
		url = generate_url("custom")
		movie_data = get_movie_data(url)
		display_movies(movie_data)
		
	elif search_option == "2":
		top_by_what = input("\nPlease define a group of top movies:\n\t1. Best movies of all time\n\t2. Best movies by year\n\t3. Best movies by genre\nYour choice (1): ")
		if top_by_what == "1" or top_by_what == "":
			url = "https://www.rottentomatoes.com/top/bestofrt/"
			soup_movies(url)
				
		elif top_by_what == "2":
			current_year = datetime.datetime.now().year
			try:
				year = int(input("\nPlease insert a year (" + str(current_year) + "): "))
			except ValueError:
				print("\nYear must be an integer! Exiting.\n")
				quit()
			if year == "":
				year = current_year

			if year >=  0 and year <= current_year:
				url = "https://rottentomatoes.com/top/bestofrt/?year=" + str(year)

			else:
				print("\nInvalid year! Exiting.\n")
				quit()

			soup_movies(url)

		elif top_by_what == "3":
			genre = input("\nPlease select a genre:\n\t1. Action & Adventure\n\t2. Animation\n\t3. Art House & International\n\t4. Classics\n\t5. Comedy\n\t6. Documentary\n\t7. Drama\n\t8. Horror\n\t9. Kids & Family\n\t10. Musical & Performing Arts\n\t11. Mystery & Suspense\n\t12. Romance\n\t13. Science Fiction & Fantasy\n\t14. Special Interest\n\t15. Sports & Fitness\n\t16. Television\n\t17. Western\nYour choice (1): ")

			if genre == "1" or genre == "":
				genre = "action__adventure"
			elif genre == "2":
				genre = "animation"
			elif genre == "3":
				genre = "art_house__international"
			elif genre == "4":
				genre = "classics"			
			elif genre == "5":
				genre = "comedy"			
			elif genre == "6":
				genre = "documentary"			
			elif genre == "7":
				genre = "drama"			
			elif genre == "8":
				genre = "horror"			
			elif genre == "9":
				genre = "kids__family"			
			elif genre == "10":
				genre = "musical__performing_arts"			
			elif genre == "11":
				genre = "mystery__suspense"			
			elif genre == "12":
				genre = "romance"			
			elif genre == "13":
				genre = "science_fiction__fantasy"			
			elif genre == "14":
				genre = "special_interest"			
			elif genre == "15":
				genre = "sports__fitness"			
			elif genre == "16":
				genre = "television"			
			elif genre == "17":
				genre = "western"
			else:
				print("\nInvalid option! Exiting.\n")
				quit()

			url = "https://www.rottentomatoes.com/top/bestofrt/top_100_" + genre + "_movies/"
			soup_movies(url)
		else:
			print("\nInvalid option! Exiting.\n")
			quit()	
	else:
		print("\nInvalid option! Exiting.\n")
		quit()
else:
	print("\nInvalid option! Exiting.\n")
	quit()
