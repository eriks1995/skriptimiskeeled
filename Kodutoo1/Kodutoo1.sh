#!/bin/bash
#==========================================================================
# Skript eeldab jargnevat:
# 1) sisend on kujul "FQDN-nimi ip-aadress" (ilma jutumarkideta)
#    naiteks "www.itcollege.ee. 214.22.46.50".
# 2) kasutuses on ainult klass C vorgud
# 3) tsoonifaile hoitakse kataloogis /etc/bind/zones
# 4) kui kirje on juba olemas failis /etc/bind/named.conf.local, siis
#    on olemas juba ka vastav tsoonifail
# Kui sisendiks anda faili asukoht (relatiivne voi absoluutne), siis
# nimetatud failis peab iga kirje olema eraldi real ja sama formaadiga
# nagu eelkirjeldatud sisend.
#==========================================================================


#--------------------------------------------------------------------------
# Funktsioon kontrollib, kas ip-aadress voi taielik domeeninimi on vigased.
# $1 - domeeninimi, $2 - aadress
# Tagastab 1 (echo): kui uks sisenditest on vigane
# Tagastab 0: kui molemad sisendid on korrektsed
#--------------------------------------------------------------------------
function kontroll {

	if ! [[ ("$2" =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$) && ("$1" =~ ^[a-z]+\.[a-z]+\.[a-z]+\.$) ]]
		then
		echo 1
	else
		echo 0
	fi
} 


#--------------------------------------------------------------------------
# Funktsioon lisab kirje serverisse eeldusel, et kirje pole vigane.
# $1 - domeeninimi, $2 - aadress
# 
# 1. Kontroll, kas tsoon on defineeritud failis /etc/hosts. Kui ei, siis 
#    valjume funktsioonist.
# 2. Kontroll, kas failis /etc/hosts defineeritud tsooni vorguaadress
#    kattub kirje vorguaadressiga. Kui ei, siis valjume funktsioonist
# 3. Kontroll, kas tsoon on defineeritud failis /etc/bind/named.conf.local.
#    Kui ei, siis lisame.
# 4. Kontroll, kas reverse tsoon on defineeritud failis
#    /etc/bind/named.conf.local. Kui ei, siis lisame.
# 5. Hostname'i ja IP-aadressi olemasolu kontroll tsoonifailis. Kirjete
#    lisamine tsoonifailidesse, vajadusel kasutajalt tagasisidet kusides.
#--------------------------------------------------------------------------
function kirjetootlus {

# $localip - serveri enda IP aadress (liidese aadress, mis on defineeritud
#            failis /etc/hosts)
# $tsoonp - tsooninimi koos lopetava punktiga.
# $tsoon - tsooninimi ilma lopetava punktita.
# $host - lisatava kirje hostname
# $addr - lisatava kirje aadress
# $revaddr - tsooni vorguaadress tagurpidisel kujul.
	tsoonp=$(echo $1 | grep -E -o "[a-z]+\.[a-z]+\.$")
	tsoon=${tsoonp::-1}
	localip=$(grep -E -o "^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+\s+$HOSTNAME\.$tsoon" /etc/hosts | awk '{print$1}')
	addr=$2
	revaddr=$(echo $2 | awk -F'.' '{print$3"."$2"."$1}')
	host=$(echo $1 | grep -E -o "^[a-z]+")

# /etc/hosts failiga seotud kontrollid
	if [ "$localip" == "" ]
		then
		echo -e "\tTsoon ei eksisteeri failis /etc/hosts. Kirje lisamine peatatud!"
		return 1
	fi
	if [ "$(echo $localip | awk -F'.' '{print $1"."$2"."$3}')" != "$(echo $addr | awk -F'.' '{print $1"."$2"."$3}')" ]
		then
		echo -e "\tKirje vorguaadress ei uhti vorguaadressiga defineeritud\n\tfailis /etc/hosts! Kirje lisamine peatatud!"
		return 1
	fi

# Kui tsoon ei eksisteeri serveris, loome temale kirje faili
# named.conf.local ja loome tsoonifaili.
	grep "zone \"$tsoon\"" /etc/bind/named.conf.local > /dev/null
	if [ $? -ne 0 ]
		then
		echo -e "\nzone \"$tsoon\" {\n\ttype master;\n\tfile \"/etc/bind/zones/db.$tsoon\";\n};" >> /etc/bind/named.conf.local
		mkdir /etc/bind/zones 2>/dev/null
		head -n -3 /etc/bind/db.local > /etc/bind/zones/db.$tsoon
		sed -i "s/local loopback interface/$tsoon/" /etc/bind/zones/db.$tsoon
		sed -i "s/localhost./$HOSTNAME.$tsoonp/" /etc/bind/zones/db.$tsoon
		sed -i "s/root.localhost./$HOSTNAME.$tsoonp/g" /etc/bind/zones/db.$tsoon
		echo -e "$tsoonp\tIN\tNS\t$HOSTNAME.$tsoonp\n$tsoonp\tIN\tA\t$localip\n$HOSTNAME\tIN\tA\t$localip" >>  /etc/bind/zones/db.$tsoon
		echo -e "\t/etc/bind/zones/db.$tsoon loodud"
	fi
	
# Kui reverse tsooni ei eksisteeri serveris, loome temale kirje faili
# named.conf.local ja loome reverse tsooni faili.
	grep "zone \"$revaddr.in-addr.arpa\"" /etc/bind/named.conf.local > /dev/null
	if [ $? -ne 0 ]
		then
		echo -e "\nzone \"$revaddr.in-addr.arpa\" {\n\ttype master;\n\tfile \"/etc/bind/zones/db.$revaddr\";\n};" >> /etc/bind/named.conf.local
		head -n -2 /etc/bind/db.127 > /etc/bind/zones/db.$revaddr
		sed -i "s/local loopback interface/$tsoon/g" /etc/bind/zones/db.$revaddr
		sed -i "s/localhost./$HOSTNAME.$tsoonp/" /etc/bind/zones/db.$revaddr
		sed -i "s/root.localhost./$HOSTNAME.$tsoonp/g" /etc/bind/zones/db.$revaddr
		echo -e "\tIN\tNS\t$HOSTNAME.\n$(echo $localip | awk -F'.' '{print $4}')\tIN\tPTR\t$HOSTNAME.$tsoonp" >> /etc/bind/zones/db.$revaddr 
		echo -e "\t/etc/bind/zones/db.$revaddr loodud"
	fi

# Kontrollime, kas sisendiks antud hostname on juba tsoonifailis olemas.
# Seejarel kontrollime, kas sisendiks antud IP aadress on tsoonifailis olemas.
# Kui on voimalus millegi asendamiseks (konflikti korral), siis
# kasutaja tagasiside abil seda teeme.
	grep -E "^$host\s+IN\s+A\s+" /etc/bind/zones/db.$tsoon > /dev/null
	if [ $? -eq 0 ] 
		then
		grep -E "^$host\s+IN\s+A\s+$addr$" /etc/bind/zones/db.$tsoon > /dev/null
		if [ $? -eq 0 ]
			then
			echo -e "\tKirje juba eksisteerib."
		else
			read -p "	Sellise hostname'iga kirje on juba olemas.
	Kas soovite olemasoleva IP aadressi asendada uuega? (jah/ei)" valik < /dev/tty
			if [ "$valik" == "jah" ]
				then
				sed -i -r "s/$host\s+IN\s+A\s+.+/$host\tIN\tA\t$addr/" /etc/bind/zones/db.$tsoon
				sed -i -r "s/[0-9]+\s+;\s+Serial/$(date "+%Y%m%d%k%M%S")\t; Serial/g" /etc/bind/zones/db.$tsoon
				echo -e "\t/etc/bind/zones/db.$tsoon muudetud"
				sed -i -r "s/[0-9]{1,3}\s+IN\s+PTR\s+$host.$tsoonp/$(echo $addr | awk -F'.' '{print$ 4}')\tIN\tPTR\t$host.$tsoonp/" /etc/bind/zones/db.$revaddr
				sed -i -r "s/[0-9]+\s+;\s+Serial/$(date "+%Y%m%d%k%M%S")\t; Serial/g" /etc/bind/zones/db.$revaddr
				echo -e "\t/etc/bind/zones/db.$revaddr muudetud"
			fi
			
		fi
	else
		grep -E "\s+IN\s+A\s+$addr$" /etc/bind/zones/db.$tsoon > /dev/null
		if [ $? -eq 0 ]
			then
			read -p "	Sellise IP aadressiga kirje on juba olemas.
	Kas soovite olemasoleva hostname'i asendada uuega? (jah/ei)" valik < /dev/tty
			if [ "$valik" == "jah" ]
				then
				sed -i -r "s/.+\s+IN\s+A\s+$addr/$host\tIN\tA\t$addr/" /etc/bind/zones/db.$tsoon
				sed -i -r "s/[0-9]+\s+;\s+Serial/$(date "+%Y%m%d%k%M%S")\t; Serial/g" /etc/bind/zones/db.$tsoon
				echo -e "\t/etc/bind/zones/db.$tsoon muudetud"
				sed -r -r "s/$(echo $addr | awk -F'.' '{print $4}')\s+IN\s+PTR\s+.+/$(echo $addr | awk -F'.' '{print $4}')\tIN\tPTR\t$host.$tsoon./" /etc/bind/zones/db.$revaddr
				sed -i -r "s/[0-9]+\s+;\s+Serial/$(date "+%Y%m%d%k%M%S")\t; Serial/g" /etc/bind/zones/db.$revaddr
				echo -e "\t/etc/bind/zones/db.$revaddr muudetud"
			fi
		else
			echo -e "$host\tIN\tA\t$addr" >> /etc/bind/zones/db.$tsoon
			sed -i -r "s/[0-9]+\s+;\s+Serial/$(date "+%Y%m%d%k%M%S")\t; Serial/g" /etc/bind/zones/db.$tsoon
			echo -e "\tKirje faili /etc/bind/zones/db.$tsoon lisatud"
			echo -e "$(echo $addr | awk -F'.' '{print $4}')\tIN\tPTR\t$host.$tsoonp" >> /etc/bind/zones/db.$revaddr
				sed -i -r "s/[0-9]+\s+;\s+Serial/$(date "+%Y%m%d%k%M%S")\t; Serial/g" /etc/bind/zones/db.$revaddr
			echo -e "\tKirje faili /etc/bind/zones/db.$revaddr lisatud"
		fi
	fi
			
}

#\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
# START
# Kontrollib, kas sisendiks on fail voi on kirje antud kaasa kasureal.
# Seejarel funktsiooni 'kontroll' abil kontrollime sisendi formaadi oigsust.
# Iga kirje antakse tootlemiseks funktsioonile 'kirjetootlus'
# Lopetuseks teeme restardi bind9le
#//////////////////////////////////////////////////////////////////////////////////
if [ -f "$1"  ]
	then
	iter=1
	while read line
	do
		echo "$iter. Kirje '$line'"
		if [ $(kontroll $line) -eq 0 ]
			then
			kirjetootlus $line 
		else
			echo -e "\tKirje on vigane"
		fi
		echo -e "\n"
		let "iter=iter+1"
	done < $1 
else 
	if [ $(kontroll $1 $2) -eq 0 ]
		then
		kirjetootlus $1 $2
	else
		echo "Kirje '$1 $2' on vigane!"
	fi
fi 
/etc/init.d/bind9 restart > /dev/null
